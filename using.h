#ifndef USING_H_INCLUDED
#define USING_H_INCLUDED

#include <SFML/Graphics.hpp>

using Renderer = sf::RenderTarget;

#include "Renderer.h"

#endif // USING_H_INCLUDED
